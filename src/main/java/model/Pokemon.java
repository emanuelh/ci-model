package model;

public class Pokemon {
	
	private String name;
	private int mana;
	private int life;
	private String dev;
	
	public boolean isDead() {
		return life <= 0;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getLife() {
		return life;
	}

	public void setLife(int life) {
		this.life = life;
	}
	
	public void setMana(int life) {
		this.life = life;
	}

	public int getMana() {
		return mana;
	}

	public String getDev() {
		return dev;
	}

	public void setDev(String dev) {
		this.dev = dev;
	}

	
	
	

}